package org.team1540.phidget.driver;

import com.phidgets.*;
import com.phidgets.event.*;
import java.io.IOException;
import java.util.Arrays;

public final class PhidgetReader implements AttachListener, DetachListener, ErrorListener, InputChangeListener, SensorChangeListener {

    public static TextLCDPhidget lcd;
    public static InterfaceKitPhidget ifa;
    private TCPHandler hand;
    private GUI readout;
    public static final int O_INSERT_LCD = 0, O_INSERT_IFA = 1, O_REMOVE_LCD = 2, O_REMOVE_IFA = 3, O_INPUT_ON = 4, O_INPUT_OFF = 5, O_CHANGE_ANALOG = 6, O_RUN_COMMAND = 7, O_UDP_PROXY = 8;
    public static final int I_LCD_LINE = 0, I_OUTPUT_ON = 1, I_OUTPUT_OFF = 2, I_PING = 3, I_CONSOLE = 4, I_BEEP = 5;

    public PhidgetReader(TCPHandler hand, GUI error) {
        try {
            this.hand = hand;
            this.readout = error;
            lcd = new TextLCDPhidget();
            ifa = new InterfaceKitPhidget();
            lcd.addAttachListener(this);
            lcd.addDetachListener(this);
            lcd.addErrorListener(this);
            ifa.addAttachListener(this);
            ifa.addDetachListener(this);
            ifa.addErrorListener(this);
            ifa.addInputChangeListener(this);
            ifa.addSensorChangeListener(this);
            lcd.openAny();
            ifa.openAny();
            lcd.setBacklight(true);
            lcd.setContrast(100);
            hand.attachReader(this);
        } catch (PhidgetException ex) {
            readout.handleError("initialize", ex);
        }
    }

    public void sendMessage(int msgtype, byte[] extra) {
        try {
            hand.sendMessage(msgtype, extra);
        } catch (IOException ex) {
            readout.handleError("Send message", ex);
        }
    }

    public void sendMessage(int msgtype) {
        sendMessage(msgtype, new byte[0]);
    }

    public void sendMessage(int msgtype, String extra) {
        sendMessage(msgtype, extra.getBytes());
    }

    private static short fixByte(byte b) {
        if (b < 0) {
            return (short) (256 + b);
        } else {
            return b;
        }
    }

    public void recieveMessage(int message, byte[] extra) {
        if (extra.length < 1 || (message == I_PING && extra.length < 3)) {
            readout.handleError("Too short message", null);
        }
        try {
            switch (message) {
                case I_LCD_LINE: {
                    String str = new String(extra, 1, extra.length - 1);
                    if (str.length() > 20) {
                        str = str.substring(0, 20);
                    }
                    lcd.setDisplayString(extra[0], str);
                    break;
                }
                case I_OUTPUT_ON:
                    ifa.setOutputState(extra[0], true);
                    break;
                case I_OUTPUT_OFF:
                    ifa.setOutputState(extra[0], false);
                    break;
                case I_PING:
                    readout.setLastPingTime(fixByte(extra[0]) + (fixByte(extra[1]) << 8) + (fixByte(extra[2]) << 16));
                    break;
                case I_CONSOLE:
                    readout.printLine(new String(extra));
                    break;
                case I_BEEP:
                    Beeper.beep(new String(extra));
                    break;
                default:
                    readout.handleError("Invalid message receive", null);
            }
        } catch (PhidgetException ex) {
            readout.handleError("Receive message", ex);
        }
    }

    @Override
    public void attached(AttachEvent ae) {
        Phidget p = ae.getSource();
        if (p == lcd) {
            try {
                assert lcd.getColumnCount() <= Byte.MAX_VALUE;
                assert lcd.getRowCount() <= Byte.MAX_VALUE;
                lcd.setBacklight(true);
                lcd.setContrast(100);
                if (this.readout.isRecentlyConnected()) {
                    sendMessage(O_INSERT_LCD, new byte[]{(byte) lcd.getColumnCount(), (byte) lcd.getRowCount()});
                }
            } catch (PhidgetException ex) {
                readout.handleError("Error on LCD attach", ex);
            }
        } else if (p == ifa) {
            try {
                assert ifa.getInputCount() <= Byte.MAX_VALUE;
                for (int i = 0; i < ifa.getInputCount(); i++) {
                    System.out.println("Old rate for: " + i + " is " + ifa.getDataRate(i));
                    ifa.setDataRate(i, 20);
                }
                if (this.readout.isRecentlyConnected()) {
                    sendMessage(O_INSERT_IFA, new byte[]{(byte) ifa.getInputCount(), (byte) ifa.getOutputCount(), (byte) ifa.getSensorCount()});
                    resendValues();
                }
            } catch (PhidgetException ex) {
                readout.handleError("Error on IFA attach", ex);
            }
        } else {
            readout.handleError("Unknown Phidget attach", null);
        }
        recalculateAttached();
    }

    @Override
    public void detached(DetachEvent ae) {
        if (this.readout.isRecentlyConnected()) {
            Phidget p = ae.getSource();
            if (p == lcd) {
                sendMessage(O_REMOVE_LCD);
            } else if (p == ifa) {
                sendMessage(O_REMOVE_IFA);
            } else {
                readout.handleError("Unknown Phidget detach", null);
            }
            recalculateAttached();
        }
    }

    private void recalculateAttached() {
        try {
            readout.setPhidgetConnected(lcd.isAttached() && ifa.isAttached());
        } catch (PhidgetException ex) {
            readout.handleError("Could not calculate attached", ex);
        }
    }

    @Override
    public void error(ErrorEvent ae) {
        readout.handleError("Phidget error message", ae.getException());
    }

    public void gotUDPProxy(byte[] buf) {
        if (this.readout.isRecentlyConnected()) {
            sendMessage(O_UDP_PROXY, buf);
        }
    }

    @Override
    public void inputChanged(InputChangeEvent ae) {
        if (this.readout.isRecentlyConnected()) {
            assert ae.getIndex() <= Byte.MAX_VALUE;
            if (ae.getState()) {
                sendMessage(O_INPUT_ON, new byte[]{(byte) ae.getIndex()});
            } else {
                sendMessage(O_INPUT_OFF, new byte[]{(byte) ae.getIndex()});
            }
        }
    }

    @Override
    public void sensorChanged(SensorChangeEvent ae) {
        if (this.readout.isRecentlyConnected()) {
            assert ae.getIndex() <= Byte.MAX_VALUE;
            int val = ae.getValue();
            assert val >= 0 && val <= 1000;
            sendMessage(O_CHANGE_ANALOG, new byte[]{(byte) ae.getIndex(), (byte) val, (byte) (val >> 8)});
        }
    }

    public void sendCrioCommand(String arg) {
        sendMessage(O_RUN_COMMAND, arg.getBytes());
    }

    public void resendAll() {
        try {
            if (lcd.isAttached()) {
                assert lcd.getColumnCount() <= Byte.MAX_VALUE;
                assert lcd.getRowCount() <= Byte.MAX_VALUE;
                lcd.setBacklight(true);
                lcd.setContrast(100);
                sendMessage(O_INSERT_LCD, new byte[]{(byte) lcd.getColumnCount(), (byte) lcd.getRowCount()});
            }
        } catch (PhidgetException ex) {
            readout.handleError("Error on LCD attach", ex);
        }
        try {
            if (ifa.isAttached()) {
                assert ifa.getInputCount() <= Byte.MAX_VALUE;
                sendMessage(O_INSERT_IFA, new byte[]{(byte) ifa.getInputCount(), (byte) ifa.getOutputCount(), (byte) ifa.getSensorCount()});
                resendValues();
            }
        } catch (PhidgetException ex) {
            readout.handleError("Error on IFA attach", ex);
        }
    }

    public void resendValues() {
        try {
            for (int i = 0; i < ifa.getInputCount(); i++) {
                if (ifa.getInputState(i)) {
                    sendMessage(O_INPUT_ON, new byte[]{(byte) i});
                } else {
                    sendMessage(O_INPUT_OFF, new byte[]{(byte) i});
                }
            }
            for (int i = 0; i < ifa.getSensorCount(); i++) {
                int val = ifa.getSensorValue(i);
                sendMessage(O_CHANGE_ANALOG, new byte[]{(byte) i, (byte) val, (byte) (val >> 8)});
            }
        } catch (PhidgetException ex) {
            readout.handleError("Error on value resend", ex);
        }
    }
}
