package org.team1540.phidget.driver;

import com.phidgets.PhidgetException;
import java.awt.EventQueue;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class CommandManager {
    private static class ExecuteEntry {
        public final String str;
        public final GUI gui;
        public final TCPHandler net;
        public final PhidgetReader pdg;
        ExecuteEntry(String str, GUI gui, TCPHandler net, PhidgetReader pdg) {
            this.str = str;
            this.gui = gui;
            this.net = net;
            this.pdg = pdg;
        }
        public void execute() {
            CommandManager.execute(str, gui, net, pdg);
        }
    }
    private static BlockingQueue<ExecuteEntry> needToRun = new ArrayBlockingQueue<ExecuteEntry>(32);
    static {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        needToRun.take().execute();
                    } catch (InterruptedException ex) {}
                }
            }
        }.start();
    }
    public static void execute(String str, GUI gui, TCPHandler net, PhidgetReader attached) {
        if (EventQueue.isDispatchThread()) {
            if (!needToRun.offer(new ExecuteEntry(str, gui, net, attached))) {
                gui.handleError("INTERNAL CommandManager ERROR: Ran out of room in execution queue!", null, "Hopefully, this shouldn't cause any problems except losing the most recent command entered into the command box. How'd you even enter that many commands without them all running?");
            }
            return;
        }
        String cmd, arg;
        if (str.startsWith("@")) {
            cmd = "@";
            arg = str.substring(1);
        } else if (str.contains(" ")) {
            cmd = str.substring(0, str.indexOf(' ')).trim();
            arg = str.substring(str.indexOf(' ')+1);
        } else {
            cmd = str.trim();
            arg = null;
        }
        PrintStream p = gui.pwr;
        if (cmd.equals("sub") || cmd.equals("subnet")) {
            p.println("Subnet Autodetector: " + IPProvider.getAddress(gui));
        } else if (cmd.equals("force-subnet")) {
            try {
                InetAddress chgto = arg == null ? null : InetAddress.getByName(arg);
                IPProvider.setForced(chgto);
                if (chgto == null) {
                    p.println("Unforced subnet address.");
                } else {
                    p.println("Forced subnet address to: " + chgto);
                }
            } catch (UnknownHostException ex) {
                gui.handleError("Error while forcing subnet: No such host: " + arg + "!", ex);
            }
        } else if (cmd.equals("target-port")) {
            if (arg == null) {
                p.println("Current port: " + IPProvider.getPort());
            } else {
                try {
                    IPProvider.port = Integer.parseInt(arg);
                } catch (NumberFormatException ex) {
                    p.println("Bad number format.");
                }
            }
        } else if (cmd.equals("rcn") || cmd.equals("reconnect")) {
            p.println("Started reconnecting...");
            net.reconnect();
            p.println("Reconnector returned.");
        } else if (cmd.equals("@") || cmd.equals("crio")) {
            if (arg == null) {
                p.println("Command required to send to cRIO!");
            } else {
                attached.sendCrioCommand(arg);
            }
        } else if (cmd.equals("setlcd")) {
            if (arg == null || !arg.contains(" ")) {
                p.println("Arguments needed for setlcd");
            } else {
                int i = arg.indexOf(' ');
                try {
                    int line = Integer.parseInt(arg.substring(0, i));
                    PhidgetReader.lcd.setDisplayString(line, arg.substring(i+1));
                } catch (PhidgetException ex) {
                    gui.handleError("set lcd line", ex);
                } catch (NumberFormatException ex) {
                    p.println("Bad format for number");
                }
            }
        } else if (cmd.equals("udp-status")) {
            p.println("UDP Status: " + UDPProxy.status());
        } else if (cmd.equals("udp-connect")) {
            p.println("Reconnecting UDP...");
            UDPProxy.reconnect(attached, gui);
            p.println("Finished reconnection process.");
        } else if (cmd.equals("udp-term")) {
            p.println("Terminating UDP...");
            if (UDPProxy.terminate()) {
                p.println("Sent termination signal.");
            } else {
                p.println("No target to terminate");
            }
        } else if (cmd.equals("h") || cmd.equals("help")) {
            if (arg == null) {
                p.println("To get more help on a command, type 'help' followed by the command.\nCommands (aliases): help (h), reconnect (rcn), subnet (sub), force-subnet, crio (@), setlcd, target-port");
            } else {
                if (arg.equals("rcn")) {
                    arg = "reconnect";
                    p.println("Alias for reconnect:");
                } else if (arg.equals("h")) {
                    arg = "help";
                    p.println("Alias for help:");
                } else if (arg.equals("sub")) {
                    arg = "subnet";
                    p.println("Alias for subnet:");
                } else if (arg.equals("@")) {
                    arg = "crio";
                    p.println("Alias for crio:");
                }
                if (arg.equals("reconnect")) {
                    p.println("reconnect: Tell the network manager to reconnect.");
                } else if (arg.equals("help")) {
                    p.println("help [command]: display a help message");
                } else if (arg.equals("subnet")) {
                    p.println("subnet: show the result of the automatic subnet detector.");
                } else if (arg.equals("force-subnet")) {
                    p.println("force-subnet [address]: if address is specified, force the result of the subnet autodetector to be that address. otherwise, clear any such forced setting.");
                } else if (arg.equals("crio")) {
                    p.println("crio command...: send command to crio. try 'crio help' to find what you can send.");
                } else if (arg.equals("setlcd")) {
                    p.println("setlcd <line> <text>: set <line> to <text>");
                } else if (arg.equals("target-port")) {
                    p.println("target-port [port]: set target port to the given port (default 80 (open on FMS), other available are generally 443 (open on FMS) and 1540 (closed on FMS)), or print out current port if none specified.");
                } else if (arg.equals("udp-status")) {
                    p.println("udp-status: provide status on the UDP Proxy");
                } else if (arg.equals("udp-connect")) {
                    p.println("udp-connect: reconnect the UDP Proxy");
                } else if (arg.equals("udp-term")) {
                    p.println("udp-term: terminate the UDP Proxy");
                } else {
                    p.println("I don't know about " + arg + "!");
                }
            }
        } else if (!cmd.isEmpty()) {
            p.println("No " + cmd + ".");
        }
    }
}
