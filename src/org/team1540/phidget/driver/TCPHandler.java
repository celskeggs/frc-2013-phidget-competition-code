package org.team1540.phidget.driver;

import java.io.*;
import java.net.*;

public final class TCPHandler extends Thread {

    private InetSocketAddress remote;
    private Socket sock;
    public PhidgetReader attached;
    private DataOutputStream dout;
    private DataInputStream din;
    private GUI readout;

    public TCPHandler(GUI error) {
        this.readout = error;
        error.setNetworkIsConnected(false);
        reconnect();
    }
    private boolean isReconnecting = false;

    public void reconnect() {
        boolean already = false;
        synchronized (this) {
            if (isReconnecting) {
                already = true;
            } else {
                isReconnecting = true;
            }
        }
        if (already) {
            readout.handleError("Already reconnecting!", null);
            return;
        }
        try {
            if (sock != null) {
                sock.close();
            }
        } catch (IOException ex) {
            readout.handleError("Cannot close socket", ex);
        }
        try {
            InetAddress in = IPProvider.getAddress(readout);
            int port = IPProvider.getPort();
            if (in == null) {
                return;
            }
            if (remote == null || !remote.getAddress().equals(in) || remote.getPort() != port) {
                readout.getOutput().println("Found new target IP: " + in + ":" + port);
            }
            remote = new InetSocketAddress(in, port);
            synchronized (this) {
                sock = null;
                dout = null;
                din = null;
            }
            readout.setNetworkIsConnected(false);
            Socket tmp = new Socket();
            tmp.connect(remote, 1000);
            synchronized (this) {
                sock = tmp;
                dout = new DataOutputStream(sock.getOutputStream());
                din = new DataInputStream(sock.getInputStream());
                this.notifyAll();
            }
            if (attached != null) {
                attached.resendAll();
            }
            readout.setNetworkIsConnected(true);
        } catch (NoRouteToHostException ex) {
            readout.handleError("Cannot connect to robot: " + ex, null, "1. Check your connection to the wireless network.\n2. Check the connection between the router and the robot.\n3. Check that the IP address " + remote.getAddress() + " is correct.\n4. Try rebooting the robot.");
        } catch (SocketTimeoutException ex) {
            readout.handleError("Cannot connect to robot: " + ex, null, "1. Check your connection to the robot\n2. Check that the IP address " + remote.getAddress() + " is correct.\n3. Try rebooting the robot.\n4. Check that TCP port " + remote.getPort() + " is open.");
        } catch (IOException ex) {
            readout.handleError("Cannot connect to robot", ex);
        } finally {
            synchronized (this) {
                isReconnecting = false;
            }
        }
    }

    public void attachReader(PhidgetReader pr) {
        attached = pr;
    }

    public void sendMessage(int type, byte[] data) throws IOException {
        try {
            DataOutputStream dl;
            synchronized (this) {
                dl = dout;
            }
            if (dl == null) {
                throw new IOException("Not currently connected");
            }
            dl.writeInt(type);
            dl.writeInt(data.length);
            dl.write(data);
        } catch (IOException ex) {
            synchronized (this) {
                sock = null;
                dout = null;
                din = null;
            }
            readout.setNetworkIsConnected(false);
            throw ex;
        }
    }

    @Override
    public void run() {
        while (true) {
            DataInputStream dl;
            synchronized (this) {
                while (din == null) {
                    try {
                        this.wait();
                    } catch (InterruptedException ex) {
                        readout.handleError("Interrupted", ex);
                    }
                }
                dl = din;
            }
            try {
                int type = dl.readInt();
                byte[] data = new byte[dl.readInt()];
                dl.readFully(data);
                attached.recieveMessage(type, data);
            } catch (IOException ex) {
                synchronized (this) {
                    sock = null;
                    din = null;
                    dout = null;
                }
                readout.setNetworkIsConnected(false);
                if (ex instanceof SocketException && ex.getMessage().equals("socket closed")) {
                    readout.getOutput().println("Socket closed successfully.");
                } else {
                    readout.handleError("Receiving", ex);
                }
            }
        }
    }
}
