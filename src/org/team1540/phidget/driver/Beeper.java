package org.team1540.phidget.driver;

import java.util.HashMap;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class Beeper {
    public static void main(String[] args) throws InterruptedException {
        beep(BeepType.CORRAL);
        Thread.sleep(5000);
    }
    public static enum BeepType {
        ALARM, ANNOY, LOW, FASTALARM, SIREN, OOPS, AIMED, CORRAL;
        private byte generateOne(long l) throws CompletedException {
            long max;
            switch (this) {
                case SIREN:
                case FASTALARM:
                case ALARM:
                case LOW:
                case ANNOY: max = 32000; break;
                case CORRAL: max = 1600; break;
                case OOPS: max = 4000; break;
                case AIMED: max = 3000; break;
                default: throw new IllegalStateException();
            }
            if (l > max) {
                throw new CompletedException();
            }
            switch (this) {
                case AIMED: return (byte)((l * ((l % 1500 < 600) ? 480 : 0)) & 0xFF);
                case OOPS: return (byte)(l * 64);
                case SIREN: return (byte)((l % 6400) * (12 + (l / 800.0) % 8));
                case FASTALARM: return (byte)((l % 800 >= 400) ? 0 : l * 66);
                case CORRAL: return (byte)((l % 1600 >= 800) ? 0 : l * 57);
                case ALARM: return (byte)((l >= 800) ? 0 : l * 57);
                case LOW: return (byte)((l % 2400 >= 1200) ? 0 : l * (6 + ((l / 2400) % 5)));
                case ANNOY: return (byte)((l % 400 >= 200) ? 0 : (l % 1200 >= 800) ? l << 7 : (l % 1200 >= 400) ? l << 4 : l << 3);
                default: throw new IllegalStateException();
            }
        }

        private float rate() {
            return this == AIMED ? 16000f : 8000f;
        }
    }
    private static final class CompletedException extends Exception {}
    private static final HashMap<String, BeepType> lookups = new HashMap<String, BeepType>();
    static {
        for (BeepType bt : BeepType.values()) {
            lookups.put(bt.name(), bt);
        }
    }
    private static BeepType current = null;
    private static final Object cursync = new Object();
    static {
        Thread t = new Thread("beeper") {
            @Override
            public void run() {
                while (true) {
                    BeepType bt;
                    synchronized (cursync) {
                        while (current == null) {
                            try {
                                cursync.wait();
                            } catch (InterruptedException ex) {}
                        }
                        bt = current;
                        current = null;
                    }
                    beep(bt);
                }
            }
        };
        t.setDaemon(true);
        t.start();
    }
    public static void beep(String name) {
        BeepType bt = lookups.get(name);
        if (bt == null) {
            bt = BeepType.OOPS;
        }
        synchronized (cursync) {
            current = bt;
            cursync.notifyAll();
        }
    }
    public static void beep(BeepType bt) {
        System.out.println("Beeping: " + bt);
        try {
            AudioFormat format = new AudioFormat(bt.rate(), 8, 1, false, true);
            SourceDataLine line = AudioSystem.getSourceDataLine(format);
            line.open(format);
            line.start();
            long time = 0;
            while (true) {
                byte[] more = new byte[1024];
                int i=0;
                try {
                    for (; i<more.length; i++) {
                        more[i] = bt.generateOne(time++);
                    }
                } catch (CompletedException ex) {
                    line.write(more, 0, i);
                    break;
                }
                line.write(more, 0, i);
            }
            line.drain();
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
        }
    }
}
