package org.team1540.phidget.driver;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.channels.ClosedByInterruptException;
import java.util.Arrays;

public class UDPProxy implements Runnable {

    private static UDPProxy instance;
    public static synchronized void reconnect(PhidgetReader target, GUI err) {
        if (instance != null && instance.running()) {
            instance.stop();
        }
        instance = null;
        try {
            instance = new UDPProxy(target, err);
        } catch (SocketException ex) {
            err.handleError("Connecting UDP Proxy", ex);
        }
    }

    public static synchronized String status() {
        if (instance == null) {
            return "No UDP Proxy instance.";
        } else if (instance.running()) {
            return "UDP Proxy is running.";
        } else {
            return "UDP Proxy is not running.";
        }
    }
    
    public static synchronized boolean terminate() {
        if (instance != null) {
            instance.stop();
            instance = null;
            return true;
        } else {
            return false;
        }
    }
    
    public final DatagramSocket sock;
    private Thread thr;
    public final PhidgetReader target;
    public final GUI err;
    private int errorcount;
    private boolean done;

    private UDPProxy(PhidgetReader target, GUI err) throws SocketException {
        sock = new DatagramSocket(1540);
        thr = new Thread(this);
        this.target = target;
        this.err = err;
        thr.start();
    }

    public boolean running() {
        return thr.isAlive();
    }

    public void stop() {
        done = true;
        thr.interrupt();
    }

    @Override
    public void run() {
        byte[] buf = new byte[300];
        DatagramPacket pk = new DatagramPacket(buf, buf.length);
        while (!done && !Thread.interrupted()) {
            try {
                sock.receive(pk);
                target.gotUDPProxy(Arrays.copyOf(buf, pk.getLength()));
                if (errorcount > 1) {
                    errorcount -= 2;
                } else if (errorcount == 1) {
                    errorcount = 0;
                }
            } catch (InterruptedIOException ex) {
                break;
            } catch (ClosedByInterruptException ex) {
                break;
            } catch (IOException ex) {
                errorcount++;
                if (errorcount >= 100) {
                    err.handleError("Terminated UDP connection (too many errors)", ex);
                    break;
                } else {
                    err.handleError("Receiving UDP packet (ec " + errorcount + ")", ex);
                }
            }
        }
        err.printLine("Terminated UDP Proxy thread.");
    }
}
