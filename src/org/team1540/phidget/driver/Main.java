package org.team1540.phidget.driver;

import java.net.SocketException;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        GUI gui = GUI.start();
        TCPHandler net = new TCPHandler(gui);
        gui.setNetworkHandler(net);
        PhidgetReader ph = new PhidgetReader(net, gui);
        net.start();
        UDPProxy.reconnect(ph, gui);
    }
}
