package org.team1540.phidget.driver;

import java.io.PrintStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;

public class IPProvider {
    public static InetAddress forced;
    public static int port = 80;
    public static void setForced(InetAddress forceto) {
        forced = forceto;
    }
    public static int getPort() {
        return port;
    }
    public static InetAddress getAddress(GUI rhand) {
        if (forced != null) {
            rhand.getOutput().println("Note: forced subnet address to " + forced);
            return forced;
        }
        Enumeration<NetworkInterface> enm;
        try {
            enm = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException ex) {
            rhand.handleError("Subnet Autodetect", ex, "I have no way to know how to fix this...");
            return null;
        }
        if (enm == null) {
            rhand.handleError("Subnet Autodetect: not connected to any network interfaces", null, "1. Check that you are connected to the wireless network.\n2. Check that your wireless adapter is enabled.");
            return null;
        }
        ArrayList<InetAddress> allAddresses = new ArrayList<InetAddress>();
        while (enm.hasMoreElements()) {
            NetworkInterface ni = enm.nextElement();
            Enumeration<InetAddress> ins = ni.getInetAddresses();
            while (ins.hasMoreElements()) {
                InetAddress addr = ins.nextElement();
                allAddresses.add(addr);
            }
        }
        for (InetAddress addr : allAddresses) {
            byte[] raw = addr.getAddress();
            if (raw.length == 4 && raw[0] == 10 && raw[1] == 15 && raw[2] >= 40 && raw[2] <= 45) {
                try {
                    return Inet4Address.getByAddress(new byte[] {raw[0], raw[1], raw[2], 2});
                } catch (UnknownHostException ex) {
                    rhand.handleError("This should never happen... apparently 4 is not 4!", ex);
                    return null;
                }
            }
        }
        PrintStream out = rhand.getOutput();
        int other = 0;
        for (InetAddress addr : allAddresses) {
            if (addr.getAddress().length == 4) {
                out.println("Found IPv4 address: " + addr);
            } else {
                other++;
            }
        }
        if (other != 0) {
            out.println("Ignored " + other + " non-IPv4 addresses");
        }
        rhand.handleError("Subnet Autodetect: Cannot find any valid network addresses!", null, "1. Make sure that you are connected to the wireless network.\n2. Check that your network adapter is enabled.\n3. Check that you have the proper IP address (either static or DHCP)");
        return null;
    }
}
